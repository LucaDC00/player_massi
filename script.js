let tracks =[
    {url:'./media/audio/capo-scloudtomp3downloader.com.mp3', cover: './media/cover_album/cover_CAPO.jpg' , artist:'NLE Choppa' , title:'CAPO' , id:'0'},
    {url:'./media/audio/Daddy (feat. Rich The Kid).mp3', cover: './media/cover_album/cover_daddy.jpg' , artist:'Blueface', title:'Daddy' , id:'1'},
    {url:'./media/audio/KOODA.mp3', cover: './media/cover_album/cover_kooda.jpg' , artist:'6ix9ine',title:'KOODA' ,id:'2'},
    {url:'./media/audio/nle-choppa-camelot-scloudtomp3downloader.com.mp3' , cover: './media/cover_album/cover_camelot.jpg' , artist:'NLE Choppa',title:'Camelot' , id:'3'},
    {url:'./media/audio/nle-choppa-drop-shit-scloudtomp3downloader.com.mp3', cover: './media/cover_album/cover_dropShit.jpg' , artist:'NLE Choppa',title:'Drop Sh*t' , id:'4'} ,
    {url:'./media/audio/Pop Smoke - Dior.mp3', cover: './media/cover_album/cover_dior.jpg' , artist:'Pop Smoke',title:'Dior' , id:'5'},
    {url:'./media/audio/shotta-flow-feat.blueface-remix-scloudtomp3downloader.com.mp3', cover: './media/cover_album/shotta_flow.jpg' , artist:'Blueface', title:'Shotta Flow Remix' , id:'6'},
    {url:'./media/audio/lambo-dreams-scloudtomp3downloader.com.mp3', cover: './media/cover_album/cover_lamboDreams.jpg' , artist:'SimpkinsTwins',title:'Lambo Dreams' , id:'7'},
    {url:'./media/audio/icy-feat.thorii-scloudtomp3downloader.com.mp3', cover: './media/cover_album/cover_icy.jpg' , artist:'ZZ',title:'ICY' , id:'8'},
    {url:'./media/audio/STOOPID (Feat. Bobby Shmurda).mp3 ', cover: './media/cover_album/cover_stoopid.jpg' , artist:'6ix9ine' ,title:'STOOPID' ,  id:'9'},
]

let currentTrack = 0
let song = tracks[currentTrack]
// let btnList = document.querySelectorAll('.btn-list')
const listWrapper = document.querySelector('#listWrapper')
const btnPlay = document.querySelector('#btnPlay')
const btnPause = document.querySelector('#btnPause')
const albumCover = document.querySelector('.album-cover')
const track = document.querySelector('#track')
const trackTitle= document.querySelector('#trackTitle')
const trackArtist = document.querySelector('#trackArtist')
const bntNext = document.querySelector('#bntNext')
const btnPrev = document.querySelector('#btnPrev')
const currentTime = document.querySelector('#currentTime')
const totalTime = document.querySelector('#totalTime')
const progressTimeBar = document.querySelector('#progressTimeBar')



// PLAYER


// console.log(progressTimeBar);



btnPlay.addEventListener('click' , ()=>{
    btnPlay.classList.toggle('d-none')
    btnPause.classList.toggle('d-none')
    albumCover.classList.toggle('play')
    track.play()

})



btnPause.addEventListener('click' , ()=>{
    btnPlay.classList.toggle('d-none')
    btnPause.classList.toggle('d-none')
    albumCover.classList.toggle('play')
    track.pause()

})


track.addEventListener('loadeddata' , ()=>{

  
    currentTime.innerHTML = '0:00'
    totalTime.innerHTML = formatTime(track.duration)

})


track.addEventListener('timeupdate' , ()=>{

    updateProgress()

})

function changeTrack(){

    let song = tracks[currentTrack]


    albumCover.src = song.cover
    track.src = song.url
    trackTitle.innerHTML = song.title
    trackArtist.innerHTML = song.artist
  


}




function formatTime(sec){

    
    let minutes = Math.floor(sec/ 60)
    let seconds = Math.floor(sec - minutes * 60)

    if(seconds < 10){
        return `${minutes}:0${seconds}`
    }

    return `${minutes}:${seconds}`
}


function updateProgress(){
    let progress = (track.currentTime / track.duration) *100

    progressTimeBar.style.width= progress + '%'

    currentTime.innerHTML = formatTime(progress)
    
}




function nextTrack(){
    
    currentTrack++
    if(currentTrack > tracks.length -1){
        currentTrack = 0
    }

    let playing = !track.paused
    changeTrack()
    if (playing == true){

        track.play()
    }

   
   
}
function prevTrack(){
    
    currentTrack--

    if(currentTrack < 0){
        currentTrack = tracks.length -1
    }

    let playing = !track.paused
    changeTrack()
    if (playing == true){

        track.play()
    }
}






bntNext.addEventListener('click' , () => {
    nextTrack()
})

btnPrev.addEventListener('click' , ()=>{
    prevTrack()
})

track.addEventListener('ended' , ()=>{

    nextTrack()
    progressTimeBar.style.width= '0%'
    

})


// LISTA

function populateList(){

    tracks.forEach(track=>{
        let card = document.createElement('div');
        card.classList.add('col-12');
        card.innerHTML = 
        `
        <div class='d-flex justify-content-between align-items-center px-5 pb-0 bordino'>
        <div class="col-4">
        <h3 class="mb-0 w-33 text-white">${track.title}</h3>
        </div>
        <div class="col-4">
        <h3 class="w-33 text-white"> ${track.artist}</h3>
        </div>
      
        <button data-model="${track.url}" data-icon="0" data-cover="${track.cover}" id="playList" class="btn-list second-color-shadow>        </button> 

        <button data-model="${track.url}" id="pauseList" class="btn-list second-color-shadow d-none icon">
        <i class="fa-solid fa-circle-pause"></i>
        </button> 
      
        </div>
        `


        listWrapper.appendChild(card)

    })
}


populateList()

let playList = document.querySelectorAll('#playList')
let pauseList = document.querySelectorAll('#pauseList')

function listPlay(){
    playList.forEach(btn=>{
            btn.innerHTML = '<i class="fa-solid fa-circle-play"></i>'

            btn.addEventListener('click' , ()=>{
                url = btn.getAttribute('data-model')
                track.src = url

                playList.forEach(btn => {
                    btn.innerHTML = '<i class="fa-solid fa-circle-play"></i>'
                   
                });
            if ( btn.dataset.icon == '20') {
                btn.dataset.icon = '0'
                btn.innerHTML = ''
                btn.innerHTML = '<i class="fa-solid fa-circle-play"></i>'
                track.pause()
            }else{
                playList.forEach(btn => {
                    btn.dataset.icon = '0'
                });
                albumCover.src = btn.dataset.cover
                btnPlay.classList.toggle('d-none')
                btnPause.classList.toggle('d-none')
                albumCover.classList.toggle('play')
                btn.dataset.icon = '20'
                btn.innerHTML = ''
                btn.innerHTML = '<i class="fa-solid fa-circle-pause"></i>'
                track.play()
            }
            
          
        })
       

    })
}
 
    listPlay()

